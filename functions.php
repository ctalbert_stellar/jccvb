<?php


//Add thumbnail, automatic feed links and title tag support
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );

//Add content width (desktop default)
if ( ! isset( $content_width ) ) {
	$content_width = 768;
}

//Add menu support and register main menu
if ( function_exists( 'register_nav_menus' ) ) {
  	register_nav_menus(
  		array(
  		  'main_menu' => 'Main Menu'
  		)
  	);
}

// filter the Gravity Forms button type
add_filter('gform_submit_button', 'form_submit_button', 10, 2);
function form_submit_button($button, $form){
    return "<button class='button btn' id='gform_submit_button_{$form["id"]}'><span>{$form['button']['text']}</span></button>";
}

// Register sidebar
add_action('widgets_init', 'theme_register_sidebar');
function theme_register_sidebar() {
	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'id' => 'sidebar-1',
		    'before_widget' => '<div id="%1$s" class="widget %2$s">',
		    'after_widget' => '</div>',
		    'before_title' => '<h4>',
		    'after_title' => '</h4>',
		 ));
	}
}

function sidebar_2() {

	$args = array(
		'id'            => 'sidebar-2',
		'name'          => __( 'Product Descriptions Sidebar', 'text_domain' ),
		'description'   => __( 'Appears in the product descriptions page.', 'text_domain' ),
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
	);
	register_sidebar( $args );

}
add_action( 'widgets_init', 'sidebar_2' );

function sidebar_3() {

	$args = array(
		'id'            => 'sidebar-3',
		'name'          => __( 'Resources Sidebar', 'text_domain' ),
		'description'   => __( 'Appears in the resources page.', 'text_domain' ),
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
	);
	register_sidebar( $args );

}
add_action( 'widgets_init', 'sidebar_3' );

// Bootstrap_Walker_Nav_Menu setup

add_action( 'after_setup_theme', 'bootstrap_setup' );

if ( ! function_exists( 'bootstrap_setup' ) ):

	function bootstrap_setup(){

		add_action( 'init', 'register_menu' );

		function register_menu(){
			register_nav_menu( 'top-bar', 'Bootstrap Top Menu' );
		}

		class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {


			function start_lvl( &$output, $depth = 0, $args = array() ) {

				$indent = str_repeat( "\t", $depth );
				$output	   .= "\n$indent<ul class=\"dropdown-menu\">\n";

			}

			function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {

				if (!is_object($args)) {
					return; // menu has not been configured
				}

				$indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

				$li_attributes = '';
				$class_names = $value = '';

				$classes = empty( $item->classes ) ? array() : (array) $item->classes;
				$classes[] = ($args->has_children) ? 'dropdown' : '';
				$classes[] = ($item->current || $item->current_item_ancestor) ? 'active' : '';
				$classes[] = 'menu-item-' . $item->ID;


				$class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item, $args ) );
				$class_names = ' class="' . esc_attr( $class_names ) . '"';

				$id = apply_filters( 'nav_menu_item_id', 'menu-item-'. $item->ID, $item, $args );
				$id = strlen( $id ) ? ' id="' . esc_attr( $id ) . '"' : '';

				$output .= $indent . '<li' . $id . $value . $class_names . $li_attributes . '>';

				$attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
				$attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
				$attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
				$attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
				$attributes .= ($args->has_children) 	    ? ' class="dropdown-toggle" data-toggle="dropdown"' : '';

				$item_output = $args->before;
				$item_output .= '<a'. $attributes .'>';
				$item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID ) . $args->link_after;
				$item_output .= ($args->has_children) ? ' <b class="caret"></b></a>' : '</a>';
				$item_output .= $args->after;

				$output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
			}

			function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {

				if ( !$element )
					return;

				$id_field = $this->db_fields['id'];

				//display this element
				if ( is_array( $args[0] ) )
					$args[0]['has_children'] = ! empty( $children_elements[$element->$id_field] );
				else if ( is_object( $args[0] ) )
					$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );
				$cb_args = array_merge( array(&$output, $element, $depth), $args);
				call_user_func_array(array(&$this, 'start_el'), $cb_args);

				$id = $element->$id_field;

				// descend only when the depth is right and there are childrens for this element
				if ( ($max_depth == 0 || $max_depth > $depth+1 ) && isset( $children_elements[$id]) ) {

					foreach( $children_elements[ $id ] as $child ){

						if ( !isset($newlevel) ) {
							$newlevel = true;
							//start the child delimiter
							$cb_args = array_merge( array(&$output, $depth), $args);
							call_user_func_array(array(&$this, 'start_lvl'), $cb_args);
						}
						$this->display_element( $child, $children_elements, $max_depth, $depth + 1, $args, $output );
					}
						unset( $children_elements[ $id ] );
				}

				if ( isset($newlevel) && $newlevel ){
					//end the child delimiter
					$cb_args = array_merge( array(&$output, $depth), $args);
					call_user_func_array(array(&$this, 'end_lvl'), $cb_args);
				}

				//end this element
				$cb_args = array_merge( array(&$output, $element, $depth), $args);
				call_user_func_array(array(&$this, 'end_el'), $cb_args);
			}
		}
 	}
endif;

//Add in the space for the weater and search form in the nav
function add_last_nav_item($items, $args) {
  if ($args->menu == 'Main') {
        return $items."<li class='jcweather'></li><li class='my-nav-menu-search'>" . get_search_form(false) . "</li>";
  }
  return $items;
}
add_filter( 'wp_nav_menu_items', 'add_last_nav_item', 10, 2 );

//remove_filter( 'the_content', 'wpautop' );

/**
 * Load site scripts.
 */
function bootstrap_theme_enqueue_scripts() {
	$template_url = get_template_directory_uri();

	// jQuery.
	wp_enqueue_script( 'jquery' );

	// Bootstrap
	wp_enqueue_script( 'bootstrap-script', $template_url . '/js/bootstrap.js', array( 'jquery' ), null, true );

	wp_enqueue_style( 'bootstrap-style', $template_url . '/css/bootstrap.css' );

	//Custom Javascript as needed
	wp_enqueue_script( 'custom-script', $template_url . '/js/custom.js', array( 'jquery' ), null, true );

	wp_enqueue_script( 'cycle2-script', $template_url . '/js/cycle2.min.js', array( 'jquery' ), null, true );

	//Main Style
	wp_enqueue_style( 'main-style', get_stylesheet_uri() );

	// Load Thread comments WordPress script.
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

function wpbeginner_numeric_posts_nav() {

	if( is_singular() )
		return;

	global $wp_query;

	/** Stop execution if there's only 1 page */
	if( $wp_query->max_num_pages <= 1 )
		return;

	$paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
	$max   = intval( $wp_query->max_num_pages );

	/**	Add current page to the array */
	if ( $paged >= 1 )
		$links[] = $paged;

	/**	Add the pages around the current page to the array */
	if ( $paged >= 3 ) {
		$links[] = $paged - 1;
		$links[] = $paged - 2;
	}

	if ( ( $paged + 2 ) <= $max ) {
		$links[] = $paged + 2;
		$links[] = $paged + 1;
	}

	echo '<div class="navigation"><ul>' . "\n";

	/**	Previous Post Link */
	if ( get_previous_posts_link() )
		printf( '<li>%s</li>' . "\n", get_previous_posts_link() );

	/**	Link to first page, plus ellipses if necessary */
	if ( ! in_array( 1, $links ) ) {
		$class = 1 == $paged ? ' class="active"' : '';

		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );

		if ( ! in_array( 2, $links ) )
			echo '<li>…</li>';
	}

	/**	Link to current page, plus 2 pages in either direction if necessary */
	sort( $links );
	foreach ( (array) $links as $link ) {
		$class = $paged == $link ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
	}

	/**	Link to last page, plus ellipses if necessary */
	if ( ! in_array( $max, $links ) ) {
		if ( ! in_array( $max - 1, $links ) )
			echo '<li>…</li>' . "\n";

		$class = $paged == $max ? ' class="active"' : '';
		printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
	}

	/**	Next Post Link */
	if ( get_next_posts_link() )
		printf( '<li>%s</li>' . "\n", get_next_posts_link() );

	echo '</ul></div>' . "\n";

}


add_action( 'wp_enqueue_scripts', 'bootstrap_theme_enqueue_scripts', 1 );
add_action( 'init', 'custom_post_type_accommodation' );
add_action( 'admin_init', 'jccvb_register_accommodation_meta_boxes' );

function custom_post_type_accommodation() {
	$labels = array(
		'name'               => _x( 'Accommodations', 'post type general name' ),
		'singular_name'      => _x( 'Accommodation', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
		'add_new_item'       => __( 'Add New Accommodation' ),
		'edit_item'          => __( 'Edit Accommodation' ),
		'new_item'           => __( 'New Accommodation' ),
		'all_items'          => __( 'All Accommodations' ),
		'view_item'          => __( 'View Accommodation' ),
		'search_items'       => __( 'Search Accommodations' ),
		'not_found'          => __( 'No accommodations found' ),
		'not_found_in_trash' => __( 'No accommodations found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Accommodations'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Accommodations in the area',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'thumbnail' ),
		'has_archive'   => false,
		'menu_icon' => get_bloginfo('template_directory') . '/icons/hotel.png',
	);
	register_post_type( 'jccvb_accommodation', $args );
}

function jccvb_register_accommodation_meta_boxes(){
    // Check if plugin is activated or included in theme
    if ( !class_exists( 'RW_Meta_Box' ) )
        return;
    $prefix = 'jccvb_';

    $meta_boxes[] = array(
        'id'       => 'accommodation_address',
        'title'    => 'Accommodation Info',
        'pages'    => array( 'jccvb_accommodation' ),
        'context'  => 'normal',
        'priority' => 'low',
        'fields' => array(
        	array(
                'name'  => 'Accommodation Rating',
                'desc'  => 'The rating of the accommodation',
                'id'    => $prefix . 'accommodation_rating',
                'type'  => 'radio',
				'options' => array(
					'2' => '2 Stars',
					'3'	=> '3 Stars',
					'4'	=> '4 Stars',
					'5'	=> '5 Stars'

				),
                'class' => 'custom-class',
                'clone'	=> false,
            ),
            array(
                'name'  => 'Featured Accommodation',
                'desc'  => 'Is this a featured accommodation?',
                'id'    => $prefix . 'accommodation_featured',
                'type'  => 'checkbox',
				'options' => array(
					'1' => 'Featured'
				),
                'class' => 'custom-class',
                'clone'	=> false,
            ),
            array(
                'name'  => 'Address',
                'desc'  => 'Enter the accommodation\'s address.',
                'id'    => $prefix . 'accommodation_address',
                'type'  => 'textarea',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
            array(
                'name'  => 'Phone Number',
                'desc'  => 'Enter the accommodation\'s phone number.',
                'id'    => $prefix . 'accommodation_phone_number',
                'type'  => 'text',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
            array(
            	'name'  => 'Toll Free',
                'desc'  => 'Enter the accommodation\'s toll free number.',
                'id'    => $prefix . 'accommodation_toll_frew',
                'type'  => 'text',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
            array(
            	'name'  => 'Contact Email',
                'desc'  => 'Enter the accommodation\'s contact email.',
                'id'    => $prefix . 'accommodation_contact',
                'type'  => 'text',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
            array(
                'name'  => 'URL',
                'desc'  => 'Enter the link to this accommodation\'s website.',
                'id'    => $prefix . 'accommodation_url',
                'type'  => 'url',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
             array(
                'name'  => 'About',
                'desc'  => 'Enter information about the accommodation.',
                'id'    => $prefix . 'accommodation_about',
                'type'  => 'wysiwyg',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
             array(
                'name'  => 'Amenities',
                'desc'  => 'Enter a comma(,) separated list of amenities available.',
                'id'    => $prefix . 'accommodation_amenities',
                'type'  => 'textarea',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
             array(
                'name'  => 'Gallery',
                'desc'  => 'Choose up to 5 images to include.',
                'id'    => $prefix . 'accommodation_gallery',
                'type'  => 'image_advanced',
                'force_delete'	=> false,
                'max-Ifile_uploads'	=> 5,
                'class' => 'custom-class',
                'clone'	=> false,
            ),
             array(
                'name'  => 'Sort Order',
                'desc'  => 'Set the default position for the accommodation.',
                'id'    => $prefix . 'accommodation_sort',
                'type'  => 'text',
                'std'   => null,
                'class' => 'custom-class',
                'clone'	=> false,
            )
        )
    );

    foreach($meta_boxes as $meta_box){
    	new RW_Meta_Box( $meta_box );
    }
}
add_action( 'init', 'custom_post_type_attraction' );
add_action( 'init', 'custom_taxonomy_attraction_groups' );
add_action( 'admin_init', 'jccvb_register_attraction_url_meta_box' );

function custom_post_type_attraction() {
	$labels = array(
		'name'               => _x( 'Attractions', 'post type general name' ),
		'singular_name'      => _x( 'Attraction', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'book' ),
		'add_new_item'       => __( 'Add New Attraction' ),
		'edit_item'          => __( 'Edit Attraction' ),
		'new_item'           => __( 'New Attraction' ),
		'all_items'          => __( 'All Attractions' ),
		'view_item'          => __( 'View Attraction' ),
		'search_items'       => __( 'Search Attractions' ),
		'not_found'          => __( 'No Attractions found' ),
		'not_found_in_trash' => __( 'No Attractions found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Attractions'
	);
	$args = array(
		'labels'        => $labels,
		'description'   => 'Attractions in the Johnson City area',
		'public'        => true,
		'menu_position' => 5,
		'supports'      => array( 'title', 'thumbnail' ),
		'has_archive'   => false,
		'menu_icon' => get_bloginfo('template_directory') . '/icons/hiking.png',
	);
	register_post_type( 'jccvb_attraction', $args );
}

function custom_taxonomy_attraction_groups() {
	$labels = array(
		'name'                => _x( 'Groups', 'taxonomy general name' ),
		'singular_name'       => _x( 'Group', 'taxonomy singular name' ),
		'search_items'        => __( 'Search Groups' ),
		'all_items'           => __( 'All Groups' ),
		'parent_item'         => __( 'Parent Group' ),
		'parent_item_colon'   => __( 'Parent Group:' ),
		'edit_item'           => __( 'Edit Group' ),
		'update_item'         => __( 'Update Group' ),
		'add_new_item'        => __( 'Add New Group' ),
		'new_item_name'       => __( 'New Group Name' ),
		'menu_name'           => __( 'Groups' )
	);

	$args = array(
		'hierarchical'        => true,
		'labels'              => $labels,
		'show_ui'             => true,
		'show_admin_column'   => true,
		'query_var'           => true,
		'rewrite'             => array( 'slug' => 'group' )
	);
	register_taxonomy('attraction_group', 'jccvb_attraction', $args);
}

function jccvb_register_attraction_url_meta_box(){
    // Check if plugin is activated or included in theme
    if ( !class_exists( 'RW_Meta_Box' ) )
        return;
    $prefix = 'jccvb_';
    $meta_box = array(
        'id'       => 'attraction_info',
        'title'    => 'Attraction Info',
        'pages'    => array( 'jccvb_attraction' ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields' => array(
        	 array(
                'name'  => 'Address',
                'desc'  => 'Enter the attraction\'s address.',
                'id'    => $prefix . 'attraction_address',
                'type'  => 'textarea',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
            array(
                'name'  => 'Phone Number',
                'desc'  => 'Enter the attraction\'s phone number.',
                'id'    => $prefix . 'attraction_phone_number',
                'type'  => 'text',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
            array(
            	'name'  => 'Toll Free',
                'desc'  => 'Enter the attraction\'s toll free number.',
                'id'    => $prefix . 'attraction_toll_frew',
                'type'  => 'text',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
            array(
                'name'  => 'URL',
                'desc'  => 'Enter the link to this accommodation\'s website.',
                'id'    => $prefix . 'accommodation_url',
                'type'  => 'url',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
             array(
                'name'  => 'Admission',
                'desc'  => 'Enter the entry cost for the attraction',
                'id'    => $prefix . 'attration_admission',
                'type'  => 'textarea',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
            array(
                'name'  => 'Hours',
                'desc'  => 'Enter the operating hours of the attraction',
                'id'    => $prefix . 'attration_attraction',
                'type'  => 'textarea',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
             array(
                'name'  => 'About',
                'desc'  => 'Enter information about the attraction.',
                'id'    => $prefix . 'attraction_about',
                'type'  => 'wysiwyg',
                'std'   => null,
                'class' => 'custom-class',
                'clone' => false,
            ),
             array(
                'name'  => 'Gallery',
                'desc'  => 'Choose up to 5 images to include.',
                'id'    => $prefix . 'attraction_gallery',
                'type'  => 'image_advanced',
                'force_delete'	=> false,
                'max-Ifile_uploads'	=> 5,
                'class' => 'custom-class',
                'clone'	=> false,
            )
        )
    );
    new RW_Meta_Box( $meta_box );
}

?>