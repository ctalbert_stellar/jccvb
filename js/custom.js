jQuery(function($){
	function getColumnHeight(){
		var heights = $(".equal-height").map(function() {
	        return $(this).height();
	    }).get(),

	    maxHeight = Math.max.apply(null, heights);

	    $(".equal-height").height(maxHeight);
	}

	$(window).scroll(function() {
	    if ($(window).scrollTop() > 850) {
	        $('.scrolltop').show();
	    }
	    else {
	        $('.scrolltop').hide();
	    }
	});

	$("a[href='#top']").click(function() {
	  $("html, body").animate({ scrollTop: 0 }, "slow");
	  return false;
	});

	$( document ).ready(function() {
	    getColumnHeight();
	});
	$( window ).on('resize', function(){
		$('.equal-height').height('auto');
		getColumnHeight();
	});
	$(document).ready(function(){

		var slideshows = $('.cycle-slideshow').on('cycle-next cycle-prev', function(e, opts) {
		    // advance the other slideshow
		    slideshows.not(this).cycle('goto', opts.currSlide);
		});

		$('body').on('click', '#accommodation-pager-img .cycle-slide', function(){
		    var index = $('#accommodation-pager-img').data('cycle.API').getSlideIndex(this);
		    slideshows.cycle('goto', index);
		});

        $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
            event.preventDefault();
            event.stopPropagation();
            $(this).parent().siblings().removeClass('open');
            $(this).parent().toggleClass('open');
        });
		 //get the weather and put it in the main menu in place of <li class='jcweather'></li>
		$.getJSON("http://api.openweathermap.org/data/2.5/weather?id=4649401&units=imperial&APPID=7c939bc0af3d37b8ce2c4169e5995da5")
    	.done(function(data){
    		var icon = 'wi wi-owm-' + data.weather[0]['id'];
    		console.log(data.main);
			$('.jcweather').html('<i class="' + icon + '"></i><span>' + Math.round(data.main['temp']) + ' &deg;F</span>');
    	});
    });

    function initCycle() {
        var width = $(document).width(); // Getting the width and checking my layout
        if ( width < 768 ) {
            $('.events-slideshow').cycle({
                fx: 'carousel',
                speed: 600,
                manualSpeed: 100,
                slides: '.event-slide',
                next: '.cycle-next',
                prev: '.cycle-prev',
                carouselVisible: 2
            });
            // console.log('Init Mobile');
        } else if ( width > 768 && width < 980 ) {
            $('.events-slideshow').cycle({
                fx: 'carousel',
                speed: 600,
                manualSpeed: 100,
                slides: '.event-slide',
                next: '.cycle-next',
                prev: '.cycle-prev',
                carouselVisible: 3
            });
        } else {
            $('.events-slideshow').cycle({
                fx: 'carousel',
                speed: 600,
                manualSpeed: 100,
                slides: '.event-slide',
                next: '.cycle-next',
                prev: '.cycle-prev',
                carouselVisible: 4
            });
        }
    }
    initCycle();

    function reinit_cycle() {
        var width = $(window).width(); // Checking size again after window resize
        if ( width < 768 ) {
            $('.events-slideshow').cycle('destroy');
            reinitCycle(2);
        } else if ( width > 768 && width < 980 ) {
            $('.events-slideshow').cycle('destroy');
            reinitCycle(3);
        } else {
            $('.events-slideshow').cycle('destroy');
            reinitCycle(4);
        }
    }
    function reinitCycle(visibleSlides) {
        $('.events-slideshow').cycle({
            fx: 'carousel',
            speed: 600,
            manualSpeed: 100,
            slides: '.event-slide',
            next: '.cycle-next',
            prev: '.cycle-prev',
            carouselVisible: visibleSlides
        });
    }
    var reinitTimer;
    $(window).resize(function() {
        clearTimeout(reinitTimer);
        reinitTimer = setTimeout(reinit_cycle, 100); // Timeout limits the number of calculations
    });

});

