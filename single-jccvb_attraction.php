<?php
/**
 * The Template for displaying attractions
 *
 * @package WordPress
 * @subpackage JCCVB
 */

get_header(); ?>
<?php
	while ( have_posts() ) : the_post();
?>
		<div class="page-no-title">
   			<?php the_post_thumbnail('full', array( 'class' => 'banner-img' )); ?>
   		</div>
   		<div class="container" id="single-<?php the_ID(); ?>">
   			<div class="row">
<?php   $postID = get_the_ID();
	   	$qattractions = new WP_Query( array(
	   		'p'	=> $postID,
			'post_type' => 'jccvb_attraction',
			'posts_per_page' => 1,
			'orderby' => 'title',
			'order' => 'ASC'
		) );
		while ( $qattractions->have_posts() ){
			$qattractions->the_post();
			$tempAccomodations[] = array(
				'name'		=> get_the_title(),
				'type'		=> get_post_meta(get_the_ID(), 'jccvb_attraction_type', true),
				'address'	=> rwmb_meta('jccvb_attraction_address',get_the_ID()),
				'contact'	=> rwmb_meta('jccvb_attraction_contact',get_the_ID()),
				'phone'		=> rwmb_meta('jccvb_attraction_phone_number',get_the_ID()),
				'tollfree'	=> rwmb_meta('jccvb_attraction_toll_frew',get_the_ID()),
				'amenities'	=> rwmb_meta('jccvb_attraction_amenities',get_the_ID()),
				'about'		=> rwmb_meta('jccvb_attraction_about',get_the_ID()),
				'images'	=> rwmb_meta('jccvb_attraction_gallery','type=image&size=Full',get_the_ID()),
				'url'		=> rwmb_meta('jccvb_attraction_url', get_the_ID())

			);
		}
		$accomodations = array_chunk($tempAccomodations, 3, true);
		foreach($accomodations as $set): ?>
			<?php foreach($set as $accomodation): ?>
			<h1 class="text-center blue"><?=$accomodation['name'];?></h1>
			<div class="col-xs-12 col-sm-7">
				<div class="cycle-slideshow" id="attraction-main-img" data-cycle-timeout=0 data-cycle-auto-height=container>
					<?php foreach($accomodation['images'] as $image=>$key){ ?>
						<img src="<?=$key['url']?>" class="image img-responsive" style="max-height:500px;">
					<?php } ?>
				</div>
				<div class="cycle-slideshow" id="attraction-pager-img" data-cycle-timeout=0 data-cycle-fx="carousel" data-cycle-carousel-visible=3 data-cycle-carousel-fluid=true data-allow-wrap=false data-cycle-next=".small-cycle-next" data-cycle-slides="> .image" data-cycle-prev=".small-cycle-prev">
					<?php foreach($accomodation['images'] as $image=>$key){ ?>
						<img src="<?=$key['url']?>" class="image img-responsive">
					<?php } ?>
					<img src="<?php echo get_template_directory_uri();?>/images/small-cycle-prev.jpg" class="small-cycle-prev" />
					<img src="<?php echo get_template_directory_uri();?>/images/small-cycle-next.jpg" class="small-cycle-next" />
				</div>

			</div>
			<div class="col-xs-12 col-sm-5 attraction-details">
				<div class="col-xs-12 col-sm-4 no-gutter blue"><b>Address:</b></div><div class="col-xs-12 col-sm-8 no-gutter green"><b><?=$accomodation['address'];?></b></div>

				<?php if($accomodation['contact'] !== ""){ ?>
					<div class="col-xs-12 col-sm-4 no-gutter blue"><b>Contact:</b></div><div class="col-xs-12 col-sm-8 no-gutter green"><b><a href="mailto:<?=$accomodation['contact'];?>"><?=$accomodation['contact'];?></a></b></div>
					<div class="clearfix"></div>
				<?php } ?>

				<?php if($accomodation['phone'] !== ""){
					$phone = preg_replace('/\D/', '', $accomodation['phone']);
				?>
					<div class="col-xs-12 col-sm-4 no-gutter blue"><b>Phone:</b></div><div class="col-xs-12 col-sm-8 no-gutter green"><b><a href="tel:<?=$phone;?>"><?=$accomodation['phone'];?></a></b></div>
					<div class="clearfix"></div>
				<?php } ?>

				<?php if($accomodation['tollfree'] !== ""){
					$tollfree = preg_replace('/\D/', '', $accomodation['phone']);
				?>
					<div class="col-xs-12 col-sm-4 no-gutter blue"><b>Toll Free:</b></div><div class="col-xs-12 col-sm-8 no-gutter green"><b><a href="tel:<?=$tollfree;?>"><?=$accomodation['tollfree'];?></a></b></div>
					<div class="clearfix"></div>
				<?php } ?>

				<?php if($accomodation['amenities'] !== ""){
					$amenities = explode(",", $accomodation['amenities']);
				?>
					<div class="col-xs-12 col-sm-4 no-gutter blue"><b>Amenities:</b></div><div class="col-xs-12 col-sm-8 no-gutter green">
					<?php foreach($amenities as $amenity){ ?>
						<span class="btn btn-blue green"><?=$amenity;?></span>
					<?php } ?>
					</div>
					<div class="clearfix"></div>
				<?php } ?>
				<div class="col-xs-12 button-area">
					<?php $mapaddress = str_replace(" ", "+", $accomodation['address']);
						$mapaddress = str_replace(".", "", $mapaddress);
						$mapaddress = preg_replace('/\s+/', '+', $mapaddress);
					?>
					<a target="_blank" class="btn btn-blue-full white col-xs-6" href="https://google.com/maps/place/<?=$mapaddress;?>" style="margin-right:3%;">Get Directions <img src="http://visitjohnsoncitytn.com/newsite/wp-content/uploads/2015/12/curly-thing.png" alt="curly-thing" width="18" height="28" class="alignnone size-full wp-image-52"></a>
					<?php if($accomodation['url'] !== ""){ ?>
					<a target="_blank" class="btn btn-blue-full white col-xs-6" href="<?=$accomodation['url'];?>">Visit Website <img src="http://visitjohnsoncitytn.com/newsite/wp-content/uploads/2015/12/curly-thing.png" alt="curly-thing" width="18" height="28" class="alignnone size-full wp-image-52"></a>
					<?php } ?>
				</div>
				<div class="col-xs-12 share-holder">
					<div class="col-xs-8 col-xs-offset-2 col-sm-3 col-sm-offset-0 text-center blue share-title no-gutter" ><b>share on</b></div>
					<div class="col-xs-12 col-sm-9">
						<div class="col-xs-3"><a href="http://twitter.com/home?status=" title="Share on Twitter" target="_blank" class="social-link"> <img src="<?php echo get_template_directory_uri();?>/images/twitter.png"/></a></div>
						<div class="col-xs-3"><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo the_permalink() ?>" title="Share on Facebook" target="_blank" class="social-link"> <img src="<?php echo get_template_directory_uri();?>/images/facebook.png"/></a></div>
						<div class="col-xs-3"><a href="http://pinterest.com/pin/create/link/?url=<?php echo the_permalink() ?>" title="Pin on Pintrest" target="_blank" class="social-link"> <img src="<?php echo get_template_directory_uri();?>/images/twitter.png"/></a></div>
						<div class="col-xs-3"><a href="https://plus.google.com/share?url=<?php echo the_permalink() ?>" title="Share on Google+" target="_blank" class="social-link"> <img src="<?php echo get_template_directory_uri();?>/images/google+.png"/></a></div>
					</div>
				</div>

			</div>
   			<div class="col-xs-12 col-sm-6 green">
   			<h4 class="about-title">About <?=$accomodation['name'];?></h4>
   			<?=$accomodation['about'];?>
   			</div>
			<?php endforeach; ?>
		<?php endforeach;
	endwhile;
?>
		</div>
   	</div>
<img src="<?php echo get_template_directory_uri();?>/images/detail-page-bottom.jpg" alt="page-bottom" class="img-responsive" style="width:100%;" />
<?php
get_footer();
