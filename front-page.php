<?php get_header(); ?>
<div id="myCarousel" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
      <?php
      $args = array('posts_per_page' => 6,'category' => 4, 'order' => 'ASC');
      $count = 0;
      $carouselposts = get_posts($args);
	  foreach($carouselposts as $post) : setup_postdata($post);?>
	  <div class="item <?php if($count == 0){ echo 'active';} ?>">
		  <?php the_post_thumbnail('full', array( 'class' => 'img-responsive' )); ?>
		  <div class="carousel-caption">
		  	<h2><?php the_title();?></h2>
		  	<p><?php the_content();?></p>
		  </div>
	  </div>
	  <?php $count++;?>
	  <?php endforeach;?>

	  <ol class="carousel-indicators">
	  <?php	for($i=0; $i < $count; $i++){ ?>
	  	<li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="<?php if($i == 0){echo "active";}?>"></li>

	  <?php } ?>
      </ol>

	  <?php wp_reset_postdata();?>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</div>

	<?php if(have_posts()) : ?>
	   <?php while(have_posts()) : the_post(); ?>
		<div class="container-fluid no-gutter" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	 		<?php the_content(); ?>
		</div>
	   <?php endwhile; ?>
	   <div class="container-fluid events-slider-holder">
	   <div class="row">
			<div class="fancy-title green col-xs-10 col-xs-offset-1"><span>Johnson City Events</span></div>
			<div class="clearfix"></div>
			<div class="events-slideshow cycle-slideshow col-xs-12" data-cycle-fx=carousel data-cycle-timeout=0 data-cycle-carousel-visible=4 data-cycle-carousel-fluid=true data-cycle-auto-height=calc data-cycle-allow-wrap=false data-cycle-slides=".event-slide">
				<?php
				$events = EM_Events::get(array('orderby'=>'start_date,start_time'));
				foreach ( $events as $event ) { ?>
				<div class="event-slide green">
					<div class="event-slide-wrapper">
						<a href="<?php echo $event->output('#_EVENTURL');?>" class="event-date"><?php echo $event->output('#_EVENTDATES');?></a>
						<h4><?php echo $event->output('#_EVENTNAME'); ?></h4>
						<?php echo $event->output('#_EVENTEXCERPT'); ?>
					</div>
				</div>
				<?php } ?>
				<div class="cycle-next"><img src="http://visitjohnsoncitytn.com/newsite/wp-content/uploads/2015/12/curly-thing.png" alt="curly-thing" width="18" height="28" class="alignnone size-full wp-image-52"></div>
				<div class="cycle-prev"><img src="http://visitjohnsoncitytn.com/newsite/wp-content/uploads/2015/12/left-curly-thing.png" alt="curly-thing" width="18" height="28" class="alignnone size-full wp-image-52"></div>
		   </div>

		   <a class="btn btn-default btn-green btn-round col-xs-10 col-xs-offset-1 col-sm-2 col-sm-offset-5" href="/events">View All Events <img src="<?php echo get_template_directory_uri(); ?>/images/curly-thing.png" alt="curly-thing" style="margin-left:15px;" /></a>
	   </div>
	   </div>
	   </div>
	<?php else : ?>

	<div class="alert alert-info">
	  <strong>No content in this loop</strong>
	</div>

	<?php endif; ?>




<?php get_footer(); ?>