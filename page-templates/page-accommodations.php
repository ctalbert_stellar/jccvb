<?php
/**
 * Template Name: Accommodations
 *
 * @package WordPress
 * @subpackage jdmetals
 */


get_header(); ?>

<?php if(have_posts()) : ?>
   <?php while(have_posts()) : the_post(); ?>
   <div class="page-title">
   		<?php the_post_thumbnail('full', array( 'class' => 'banner-img' )); ?>
   		<h1>Places to Stay in Johnson City</h1>
   </div>

	<div id="post-<?php the_ID(); ?> accommodations-page mulitplebgs" <?php post_class(); ?>>
		<div class="col-xs-12 col-sm-9 col-sm-push-3">
		<?php
		$qAccommodations = new WP_Query( array(
			'post_type' => 'jccvb_accommodation',
			'posts_per_page' => -1,
			'orderby' => 'title',
			'order' => 'ASC'
		) );
		while ( $qAccommodations->have_posts() ){
			$qAccommodations->the_post();
			$tempAccomodations[] = array(
				'name' => get_the_title(),
				'type' => get_post_meta(get_the_ID(), 'jccvb_accommodation_type', true),
				'images' => rwmb_meta('jccvb_accommodation_gallery','type=image&size=Medium',get_the_ID()),
				'about' => rwmb_meta('jccvb_accommodation_about', get_the_ID()),
				'website' => rwmb_meta('jccvb_accommodation_url', get_the_ID()),
				'url'	=> post_permalink(get_the_ID())

			);
		}
		$accomodations = array_chunk($tempAccomodations, 3, true);
		foreach($accomodations as $set): ?>
			<?php foreach($set as $accomodation): ?>
			<div class="col-xs-12 accommodation-list">
				<?php $firstkey = key($accomodation['images']);?>
				<div class="col-xs-6 col-sm-3">
					<img src="<?=$accomodation['images'][$firstkey]['url']?>" class="image">
				</div>
				<div class="col-xs-6 col-sm-9">
					<div class="type green">Type : <?=$accomodation['type']?></div>
					<h3 class="name blue"><a href="<?=$accomodation['url']?>"><?=$accomodation['name']?></a></h3>
					<p class="accommodation-excerpt"><?php $theexcerpt = explode(PHP_EOL, $accomodation['about']); echo $theexcerpt[0]; ?></p>
					<a target="_blank" class="btn btn-blue-full white col-xs-12 col-sm-4 col-sm-push-1" href="<?=$accomodation['url'];?>" style="margin-right:3%;">View Details <img src="http://visitjohnsoncitytn.com/newsite/wp-content/uploads/2015/12/curly-thing.png" alt="curly-thing" width="18" height="28" class="alignnone size-full wp-image-52"></a>
					<?php if($accomodation['website'] !== ""){ ?>
					<a target="_blank" class="btn btn-blue-full white  col-xs-12 col-sm-4 col-sm-offset-1" href="<?=$accomodation['website'];?>">Book Now <img src="http://visitjohnsoncitytn.com/newsite/wp-content/uploads/2015/12/curly-thing.png" alt="curly-thing" width="18" height="28" class="alignnone size-full wp-image-52"></a>
					<?php } ?>
				</div>
			</div>
			<?php endforeach; ?>
		<?php endforeach; ?>
	</div>
 		<div class="col-xs-12 col-sm-3 col-sm-pull-9">
			<?php get_sidebar('3'); ?>
		</div>
	<?php
	if (is_singular()) {
		// support for pages split by nextpage quicktag
		wp_link_pages();

		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;

		// tags anyone?
		the_tags();
	}
	?>
   <?php endwhile; ?>

<?php if (!is_singular()) : ?>
	<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
	<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
<?php endif; ?>

<?php else : ?>

<div class="alert alert-info">
  <strong>No content in this loop</strong>
</div>

<?php endif; ?>

</div>
<img src="<?php echo get_template_directory_uri();?>/images/page-bottom.jpg" alt="page-bottom" class="img-responsive" style="width:100%;" />
<?php get_footer(); ?>