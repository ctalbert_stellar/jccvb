<?php
/**
 * Template Name: Page with Banner
 *
 * @package WordPress
 * @subpackage jdmetals
 */


get_header(); ?>

<?php if(have_posts()) : ?>
   <?php while(have_posts()) : the_post(); ?>
   <?php the_post_thumbnail('full', array( 'class' => 'banner-img img-responsive' )); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php the_title('<h1 class="page-title">','</h1>'); ?>
		<div class="container" style="margin-top: 30px;">
 		<?php the_content(); ?>
		</div>
	</div>
	<?php
	if (is_singular()) {
		// support for pages split by nextpage quicktag
		wp_link_pages();

		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;

		// tags anyone?
		the_tags();
	}
	?>
   <?php endwhile; ?>

<?php if (!is_singular()) : ?>
	<div class="nav-previous alignleft"><?php next_posts_link( 'Older posts' ); ?></div>
	<div class="nav-next alignright"><?php previous_posts_link( 'Newer posts' ); ?></div>
<?php endif; ?>

<?php else : ?>

<div class="alert alert-info">
  <strong>No content in this loop</strong>
</div>

<?php endif; ?>

<?php get_footer(); ?>